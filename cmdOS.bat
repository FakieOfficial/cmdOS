@echo off
SETLOCAL EnableDelayedExpansion
set version=7
set termversion=2
set "_params="
title cmdOS %version%
:start
cls
select !white "Register" "Login" "Guest" "Exit"
for /f "delims=" %%x in (selected.txt) do set answer=%%x
del selected.txt
if %answer%==Login goto Login
if %answer%==Register goto Register
if %answer%==Guest start GuestMode.bat %version%
if %answer%==Exit exit
goto :eof
:Login
cls
mkdir Accounts >NUL
mkdir Home >NUL
cd Accounts
dir /b > temp.txt
for /f "tokens=*" %%A in (temp.txt) do (
    if not %%A==temp.txt (
        set _params=!_params! %%~nA
    )
)
del temp.txt
cd..
SETLOCAL DisableDelayedExpansion
cd Applications
del *.bat >NUL
cd..
cd Accounts
if "%_params%"=="" (cd.. && goto Register) else (goto jq)
:zaz
cd..
set /p writpass=Password: 
EncryptFile.exe unlock %writpass% Accounts/%name%.acc Accounts/temp.txt
if %errorlevel%==0 (cd Accounts && goto system) else (goto qns)
:qns
echo Wrong password!
timeout /t 3 /NOBREAK>NUL
goto start
:error 
cd..
set err=USER_DOES_NOT_EXIST
goto bsod
:Register
cls
cd Accounts
set /p name=Username: 
if exist %name%.acc (set err=USER_ALREADY_EXISTS && goto bsod)
cd..
cd Home
mkdir %name%
cd %name%
type white> clr.var
cd..
cd..
cls
cd Accounts
type NUL > %name%.acc
echo Do you want to set a password?
cd..
select !white "Yes" "No"
for /f "delims=" %%x in (selected.txt) do set answer=%%x
del selected.txt
if "%answer%"=="Yes" (goto NP) else (goto skippass)
set pass="*G*"
goto skip1
:NP
cls
set /p pass=Set password: 
goto skip1
:skip1
cls
echo|set /p="success" > Accounts/encrypt.txt
EncryptFile.exe lock %pass% Accounts/encrypt.txt Accounts/%name%.acc
cd Accounts
del encrypt.txt
goto system
:skippass
cls
cd Accounts
type NUL > %name%.acc
goto system
:bsod
cls
color 1f
echo Error!
echo An error occured in cmdOS! %err%
pause >NUL
goto start
:system
goto eqe
exit
:jq
cls
echo Welcome
cd..
select !white %_params%
cls
for /f "delims=" %%x in (selected.txt) do set name=%%x
del selected.txt
cd Accounts
FOR /F "tokens=*" %%a in ('type %name%.acc') do SET Correct=%%a
if "%Correct%"=="" (goto system) else (goto zaz)
goto :eof

:eqe
if exist temp.txt (del temp.txt && goto l) else (goto l)
:l
cd "../Home/%name%"
for /f "delims=" %%x in (clr.var) do set clr=%%x
cd..
cd..
if "%clr%"=="blue" (
    color 9
)
if "%clr%"=="yellow" (
    color E
)
if "%clr%"=="green" (
    color A
)
if "%clr%"=="red" (
    color C
)
if "%clr%"=="" (
    set clr="white"
)
cls
echo Hello, %name%!
echo.
:qn
echo Here are some programs you can use.
select !%clr% "Timer" "Calculator" "Clicker" "Command Line" "Text Editor" "Settings" "About" "Quit"
for /f "delims=" %%x in (selected.txt) do set answer="%%x"
del selected.txt
cls
cd Applications
if %answer%=="Timer" (goto t)
if %answer%=="Calculator" (goto c)
if %answer%=="Clicker" (goto ld)
if %answer%=="Command Line" (goto m)
if %answer%=="Text Editor" (goto k)
if %answer%=="Settings" (goto azd)
if %answer%=="About" (goto wqw)
if %answer%=="Quit" (exit)
cls
goto :eqe
:ld
copy clicker.app clicker.bat >NUL
start /b /wait clicker.bat %name% %clr%
del clicker.bat
exit
:azd
copy settings.app settings.bat >NUL
start /b /wait settings.bat %name% %clr%
del settings.bat
exit
:t
copy timer.app timer.bat >NUL
start /b /wait timer.bat %clr%
del timer.bat
exit
:c
copy calculator.app calculator.bat >NUL
start /b /wait calculator.bat %clr%
del calculator.bat
exit
:m
copy command.app command.bat >NUL
start /b /wait command.bat %version% %name% %termversion%
del command.bat
exit
:k
copy text.app text.bat >NUL
start /b /wait text.bat %name% %clr%
del text.bat
exit
:wqw
cls
cd ..
echo Version: %version%
echo Command Line Version: %termversion%
echo Username: %name%
echo.
select %clr% "Update" "Go back"
for /f "delims=" %%x in (selected.txt) do set answer=%%x
del selected.txt
cls
if "%answer%"=="Update" (
    copy update.app update.bat
    start /b update.bat %version%
    del update.bat
    exit
) 
if "%answer%"=="Go back" (echo Hello, %name%! && echo. && goto qn)