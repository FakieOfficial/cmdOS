@echo off
title Clicker
cd..
if "%2"=="blue" (
    color 9
)
if "%2"=="yellow" (
    color E
)
if "%2"=="green" (
    color A
)
if "%2"=="red" (
    color C
)
if "%2"=="" (
    set clr="white"
)
set /a money=0
set /a x=1
set /a ox=10
set /a tx=20
set /a tnx=40
set /a fox=80
set /a tex=160
:st
cls
echo Money: $%money%
select !%2 "Click" "Shop"
for /f "delims=" %%x in (selected.txt) do set choice=%%x
del selected.txt
if "%choice%"=="Click" (set /a money=%money%+%x% && goto st)
if "%choice%"=="Shop" (goto 2)
goto :eof
:2
cls
echo You have $%money%. Pick an upgrade.
select !%2 "2x money ($%ox%)" "3x money ($%tx%)" "4x money ($%tnx%)" "5x money ($%fox%)" "10x money ($%tex%)" "Back"
for /f "delims=" %%x in (selected.txt) do set answer="%%x"
del selected.txt
set /a wb=1
if %answer%=="2x money ($%ox%)" (set /a w=%ox% && set /a wb=2)
if %answer%=="3x money ($%tx%)" (set /a w=%tx% && set /a wb=3)
if %answer%=="4x money ($%tnx%)" (set /a w=%tnx% && set /a wb=4)
if %answer%=="5x money ($%fox%)" (set /a w=%fox% && set /a wb=5)
if %answer%=="10x money ($%tex%)" (set /a w=%tex% && set /a wb=10)
if %answer%=="Back" (cls && goto st)
if %money% GEQ %w% (
	set /a money=%money%-%w%
	set /a x=%x%*%wb%
	set /a ox=%ox%*2
    set /a tx=%tx%*2
    set /a tnx=%tnx%*2
    set /a fox=%fox%*2
    set /a tex=%tex%*2
	cls
	goto st
)
else (
	cls
	echo Not enough money.
	timeout /t 2 /nobreak >NUL
    goto st
)