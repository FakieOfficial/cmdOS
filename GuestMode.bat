@echo off
:a
title cmdOS %1 (Guest)
color 0e
echo Hello, Guest!
echo.
echo Here are some programs you can use.
select !white "Timer" "Calculator" 
for /f "delims=" %%x in (selected.txt) do set answer=%%x
del selected.txt
if "%answer%"=="Timer" (cd Applications && call :t)
if "%answer%"=="Calculator" (cd Applications && call :c)
pause
cd..
cls
goto a
:t
copy timer.app timer.bat >NUL
start /b /wait timer.bat %clr%
del timer.bat
exit
:c
copy calculator.app calculator.bat >NUL
start /b /wait calculator.bat %clr%
del calculator.bat
exit